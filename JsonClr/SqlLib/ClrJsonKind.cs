using System;
using System.Collections.Generic;
using System.Text;

public enum ClrJsonKind : byte {
	Null = 0,
	True = 1,
	False = 2,
	Byte = 3,
	SignedByte = 4,
	Int16 = 5,
	UnsignedInt16 = 6,
	Int32 = 7,
	UnsignedInt32 = 8,
	Int64 = 9,
	UnsignedInt64 = 10,
	Float = 11,
	Double = 12,
	Decimal = 13,
	DateTime = 14,
	TimeSpan = 15,
	String = 16,
	Object = 17,
	Array = 18
}