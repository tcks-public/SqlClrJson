using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;

internal class InternalHelper {
	public static object ToDotNetValue(object value) {
		if (value is INullable n && n.IsNull) { return null; }

		if (value is SqlBinary sqlBinary) { return sqlBinary.Value; }
		if (value is SqlBoolean sqlBoolean) { return sqlBoolean.Value; }
		if (value is SqlByte sqlByte) { return sqlByte.Value; }
		if (value is SqlBytes sqlBytes) { return sqlBytes.Value; }
		if (value is SqlChars sqlChars) { return sqlChars.Value; }
		if (value is SqlDateTime sqlDateTime) { return sqlDateTime.Value; }
		if (value is SqlDecimal sqlDecimal) { return sqlDecimal.Value; }
		if (value is SqlDouble sqlDouble) { return sqlDouble.Value; }
		if (value is SqlGuid sqlGuid) { return sqlGuid.Value; }
		if (value is SqlInt16 sqlInt16) { return sqlInt16.Value; }
		if (value is SqlInt32 sqlInt32) { return sqlInt32.Value; }
		if (value is SqlInt64 sqlInt64) { return sqlInt64.Value; }
		if (value is SqlMoney sqlMoney) { return sqlMoney.Value; }
		if (value is SqlSingle sqlSingle) { return sqlSingle.Value; }
		if (value is SqlString sqlString) { return sqlString.Value; }
		if (value is SqlXml sqlXml) { return sqlXml.Value; }

		return value;
	}

	public static bool IsPrimitiveNumber(object dotNetValue) {
		return dotNetValue is byte
				|| dotNetValue is sbyte
				|| dotNetValue is short
				|| dotNetValue is ushort
				|| dotNetValue is int
				|| dotNetValue is uint
				|| dotNetValue is long
				|| dotNetValue is ulong
				|| dotNetValue is float
				|| dotNetValue is double
				|| dotNetValue is decimal;
	}

	public static string InvariantFormat(object value) {
		return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}", value);
	}
}