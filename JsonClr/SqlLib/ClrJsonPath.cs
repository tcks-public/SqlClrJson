using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json.Linq;

[Serializable]
[SqlUserDefinedType(Format.UserDefined, Name = "ClrJsonPath", IsFixedLength = false, MaxByteSize = -1)]
public partial struct ClrJsonPath : INullable {
	private string content;
	public bool IsNull => string.IsNullOrWhiteSpace(this.content);
	public string Str => this.ToString();

	public ClrJsonPath(string content) : this() {
		this.content = content;
	}

	public override string ToString() => this.content;

	public ClrJsonPath NotNull => this.IsNull ? Root : this;
}

#region Factory methods
partial struct ClrJsonPath {
	public static ClrJsonPath Null => new ClrJsonPath();
	public static ClrJsonPath Parse(SqlString s) => s.IsNull ? Null : new ClrJsonPath(s.Value);

	public static ClrJsonPath Root => new ClrJsonPath("$");
}
#endregion Factory methods

#region Query methods
partial struct ClrJsonPath {
	public ClrJson Query(ClrJson json) => json.Query(this);

	public ClrJsonPath Descendant() => new ClrJsonPath($"{this.content ?? "$"}..");
	public ClrJsonPath Member(string memberName) => new ClrJsonPath($"{this.content ?? "$"}.{memberName}");

	public ClrJsonPath Item(int index) => new ClrJsonPath($"{this.content ?? "$"}[{index}]");

	public ClrJsonPath ItemsEqualTo(string itemMemberName, object value) => this.ItemsFilter(itemMemberName, "==", value);
	public ClrJsonPath ItemsNotEqualTo(string itemMemberName, object value) => this.ItemsFilter(itemMemberName, "!=", value);
	public ClrJsonPath ItemsLessThan(string itemMemberName, object value) => this.ItemsFilter(itemMemberName, "<", value);
	public ClrJsonPath ItemsEqualToOrLessThan(string itemMemberName, object value) => this.ItemsFilter(itemMemberName, "<=", value);
	public ClrJsonPath ItemsGreaterThan(string itemMemberName, object value) => this.ItemsFilter(itemMemberName, ">", value);
	public ClrJsonPath ItemsEqualToGreaterThan(string itemMemberName, object value) => this.ItemsFilter(itemMemberName, ">=", value);
	public ClrJsonPath ItemsFilter(string itemMemberName, string comparision, object value) {
		var okComparision = comparision?.Trim();
		if (okComparision is null) { throw new ArgumentNullException(nameof(comparision)); }
		if (okComparision == "=") {
			return this.ItemsFilterCore(itemMemberName, "==", value);
		}
		else if (okComparision == "<>") {
			return this.ItemsFilterCore(itemMemberName, "!=", value);
		}
		else if (okComparision == "==" || okComparision == "!=" || okComparision == "<" || okComparision == "<=" || okComparision == ">" || okComparision == ">=") {
			return this.ItemsFilterCore(itemMemberName, okComparision, value);
		}

		throw new ArgumentOutOfRangeException(nameof(comparision));
	}

	private ClrJsonPath ItemsFilterCore(string itemMemberName, string comparision, object value) {
		string strValue = this.ToQueryValue(value);

		return new ClrJsonPath($"{this.content ?? "$"}[?(@.{itemMemberName} {comparision} {strValue})]");
	}

	private string ToQueryValue(object value) {
		var dotNetValue = InternalHelper.ToDotNetValue(value);

		if (dotNetValue is null) {
			return "null";
		}
		else if (dotNetValue is bool b) {
			return b ? "true" : "false";
		}
		else if (InternalHelper.IsPrimitiveNumber(dotNetValue)) {
			return InternalHelper.InvariantFormat(dotNetValue);
		}

		var strValue = InternalHelper.InvariantFormat(dotNetValue)?.Replace("'", "''");
		return $"'{strValue}'";
	}
}
#endregion Query methods

#region IBinarySerialize implementation
partial struct ClrJsonPath : IBinarySerialize {
	public void Read(BinaryReader r) {
		var version = r.ReadInt32();
		if (0 == version) {
			// null
			this.content = null;
			return;
		}

		if (1 != version) {
			throw new NotSupportedException($"The version {version} is not supported.");
		}

		var str = r.ReadString();
		this.content = str;
	}

	public void Write(BinaryWriter w) {
		var root = this.content;
		if (root is null) {
			w.Write(0);
			return;
		}

		w.Write(1);

		w.Write(root);
	}
}
#endregion IBinarySerialize implementation