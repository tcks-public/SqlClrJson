using System;
using System.Collections.Generic;
using System.Text;

namespace SqlLib {
	public struct JPathFilter {
		public readonly JPathFilterKind Kind;
		public readonly string Value;

		public JPathFilter(JPathFilterKind kind, string value) {
			this.Kind = kind;
			this.Value = value;
		}
	}
}
