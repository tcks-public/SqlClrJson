using System;
using System.Collections.Generic;
using System.Text;

namespace SqlLib {
	/// <summary>
	/// This is wrapper arount original 'JPath' class which is internal for some reason.
	/// </summary>
	public class JPathWrapper {
		private readonly object original;

		public JPathWrapper(string expression) {
			var tpJPath = typeof(Newtonsoft.Json.Linq.JArray).Assembly.GetType("Newtonsoft.Json.Linq.JsonPath.JPath", true, false);
			this.original = Activator.CreateInstance(tpJPath, new object[] { expression });
		}

		public IEnumerable<JPathFilter> GetFilters() {
			var propFilters = original.GetType().GetProperty("Filters", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
			var objFilters = propFilters.GetValue(original);
			if (null == objFilters) { yield break; }

			var filters = (System.Collections.IEnumerable)objFilters;
			foreach (var objFilter in filters) {
				var tpFilter = objFilter.GetType();
				var tpFilterName = tpFilter.Name;
				if (tpFilterName == "FieldFilter") {
					var propName = tpFilter.GetProperty("Name", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
					var name = (string)propName.GetValue(objFilter);
					yield return new JPathFilter(JPathFilterKind.Field, name);
				}
				else if (tpFilterName == "ArrayIndexFilter") {
					var propIndex = tpFilter.GetProperty("Index", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
					var index = (int)propIndex.GetValue(objFilter);
					var str = string.Format(System.Globalization.CultureInfo.InvariantCulture, "[{0}]", index);
					yield return new JPathFilter(JPathFilterKind.ArrayIndex, str);
				}
				else {
					var errMsg = string.Format("Filter kind '{0}' is not supported.", tpFilterName);
					throw new NotSupportedException(errMsg);
				}
			}
		}
	}
}
