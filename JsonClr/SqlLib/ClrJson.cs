using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Text;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SqlLib;

[Serializable]
[SqlUserDefinedType(Format.UserDefined, Name = "ClrJson", IsFixedLength = false, MaxByteSize = -1)]
public partial struct ClrJson : INullable {
	private JToken content;
	public bool IsNull => (content is null);
	public string Str => this.ToString();

	public ClrJson(JToken content) : this() {
		this.content = content;
	}

	public override string ToString() {
		var root = this.content;
		if (root is null) { return null; }

		if (root is JValue jv) {
			return InternalHelper.InvariantFormat(jv.Value);
		}

		return root.ToString(Formatting.None);
	}

	public ClrJson Clone() {
		var c = this.content;
		if (null == c) {
			return new ClrJson();
		}

		var clone = c.DeepClone();
		return new ClrJson(clone);
	}
}

#region Factory methods
partial struct ClrJson {
	public static ClrJson Null => new ClrJson();

	public static ClrJson Parse(SqlString s) {
		try {
			if (s.IsNull) { return Null; }

			var str = s.Value?.Trim();
			if (string.IsNullOrEmpty(str)) { return Null; }

			using (var sr = new StringReader(s.Value)) {
				using (var jr = new JsonTextReader(sr)) {
					var obj = JsonSerializer.Create().Deserialize(jr);
					JToken token = obj as JToken;
					if (token is null) {
						token = new JValue(obj);
					}

					return new ClrJson(token);
				}
			}
		}
		catch (Exception exc) {
			throw new InvalidOperationException($"Error during parsing ({s}).", exc);
		}
	}
}
#endregion Factory methods

#region JSON related members
partial struct ClrJson {
	public bool IsValue => this.content is JValue;
	public bool IsArray => this.content is JArray;
	public bool IsObject => this.content is JObject;

	public int? Length => (this.content as JContainer)?.Count;
}
#endregion JSON related members

#region Query methods
partial struct ClrJson {
	public ClrJson First() {
		var root = this.content;
		if (root is null) {
			// this IS NULL
			return this;
		}

		if (root is JArray) {
			return new ClrJson(root.First);
		}

		return this;
	}

	public ClrJson Last() {
		var root = this.content;
		if (root is null) {
			// this IS NULL
			return this;
		}

		if (root is JArray) {
			return new ClrJson(root.Last);
		}

		return this;
	}

	public ClrJson Item(int index) {
		var root = this.content;
		if (root is null) {
			// this IS NULL
			return this;
		}

		if (root is JArray arr) {
			return new ClrJson(arr[index]);
		}

		return Null;
	}

	public ClrJson Range(int index, int length) {
		var root = this.content;
		if (root is null) {
			// this IS NULL
			return this;
		}

		if (root is JArray arr) {
			var resultArray = new JArray();

			var min = Math.Max(0, index);
			var max = Math.Min(index + length, arr.Count);
			for (var i = min; i < max; i++) {
				resultArray.Add(arr[i]);
			}

			return new ClrJson(resultArray);
		}

		return Null;
	}

	public ClrJson ToArray() {
		if (this.IsNull || this.IsArray) { return this; }

		var result = new JArray(this.content);
		return new ClrJson(result);
	}

	public ClrJson Q(string jsonPath) => this.Query(new ClrJsonPath(jsonPath));
	public ClrJson Query(ClrJsonPath jsonPath) {
		var root = this.content;
		if (root is null) {
			// this IS NULL
			return this;
		}

		var path = jsonPath.Str;
		if (string.IsNullOrWhiteSpace(path)) {
			// returns root
			return this;
		}

		var node = root as JContainer;
		if (node is null) {
			// nothing to query
			return Null;
		}

		var resultArray = new JArray();
		foreach (var token in node.SelectTokens(path)) {
			resultArray.Add(token);
		}

		var len = resultArray.Count;
		if (len < 1) {
			return Null;
		}
		else if (len < 2) {
			return new ClrJson(resultArray[0]);
		}

		return new ClrJson(resultArray);
	}
}
#endregion Query methods

#region Modification methods
partial struct ClrJson {
	public ClrJson ClearNulls(string path) {
		if (this.IsNull) { return this; }

		var clone = this.Clone();

		var root = clone.content as JContainer;
		if (null == root) { return this; }

		var node = root.SelectToken(path);
		var arrNode = node as JArray;
		if (null == arrNode) { return this; }

		for (var i = arrNode.Count - 1; i >= 0; i--) {
			var itemNode = arrNode[i];
			if (itemNode.Type == JTokenType.Null) {
				itemNode.Remove();
			}
		}

		return clone;
	}
}

partial struct ClrJson {
	public ClrJson Concat(ClrJson other) {
		var isNull1 = this.IsNull;
		var isNull2 = other.IsNull;

		if (isNull1) {
			if (isNull2) {
				return Null;
			}

			return other.ToArray();
		}
		else if (isNull2) {
			return this.ToArray();
		}

		var result = new JArray();
		if (this.content is JArray arr1) {
			foreach (var item in arr1.Children()) {
				result.Add(item);
			}
		}
		else {
			result.Add(this.content);
		}

		if (other.content is JArray arr2) {
			foreach (var item in arr2.Children()) {
				result.Add(item);
			}
		}
		else {
			result.Add(other.content);
		}

		return new ClrJson(result);
	}
}

partial struct ClrJson {
	private JToken EnsureInternal(ClrJsonPath jsonPath) {
		var c = this.content;
		JToken node = null;
		if (null != c) {
			node = c.SelectToken(jsonPath.Str);
		}

		if (null != node) { return node; }

		var path = new JPathWrapper(jsonPath.Str);

		var currNode = this.content;
		foreach (var filter in path.GetFilters()) {
			var nextNode = currNode.SelectToken(filter.Value);
			if (null != nextNode) {
				currNode = nextNode;
				continue;
			}

			var fKind = filter.Kind;
			if (fKind == JPathFilterKind.Field) {
				var prop = new JProperty(filter.Value, null);
				if (currNode.Type == JTokenType.Null) {
					var tmpNode = new JObject();
					currNode.Replace(tmpNode);
					currNode = tmpNode;
				}

				((JObject)currNode).Add(prop);

				currNode = prop.Value;
				continue;
			}
			else if (fKind == JPathFilterKind.ArrayIndex) {
				if (currNode.Type == JTokenType.Null) {
					var tmpNode = new JArray();
					currNode.Replace(tmpNode);
					currNode = tmpNode;
				}

				var arr = (JArray)currNode;
				var strIndex = filter.Value.Replace("[", "").Replace("]", "");
				var index = int.Parse(strIndex, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture);

				if (arr.Count > index) {
					arr[index] = nextNode = new JValue((object)null);
				}
				else {
					while (arr.Count < index) {
						arr.Add(new JValue((object)null));
					}

					arr.Add(nextNode = new JValue((object)null));
				}

				currNode = nextNode;
				continue;
			}
			else {
				var errMsg = string.Format("Path kind '{0}' is not supported.", filter.Kind);
				throw new NotSupportedException(errMsg);
			}
		}

		return currNode;
	}

	public ClrJson Ensure(ClrJsonPath jsonPath) {
		var result = this.Clone();
		result.EnsureInternal(jsonPath);
		return result;
	}
}

partial struct ClrJson {
	public ClrJson Add(string jsonPath, string item) {
		var root = this.Clone();
		var node = root.EnsureInternal(ClrJsonPath.Parse(jsonPath));

		var itemContent = ClrJson.Parse(item).content;
		if (null == itemContent) { return root; }

		var nodeType = node.Type;
		if (nodeType == JTokenType.Array) {
			var arrNode = (JArray)node;
			arrNode.Add(itemContent);
		}
		else if (nodeType == JTokenType.Null) {
			var newArray = new JArray(itemContent);
			node.Replace(newArray);
		}
		else {
			var errMsg = string.Format("Can not add to node type '{0}'.", nodeType);
			throw new NotSupportedException(errMsg);
		}

		return root;
	}
}

partial struct ClrJson {
	public ClrJson Set(string jsonPath, object content) {
		if (content is SqlString) { content = content.ToString(); }

		var result = this.Clone();
		var node = result.EnsureInternal(ClrJsonPath.Parse(jsonPath));

		if (content is ClrJson) {
			var jsonContent = ((ClrJson)content).content;
			node.Replace(jsonContent);
		}
		else if (node is JValue) {
			((JValue)node).Value = content;
		}
		else {
			throw new NotImplementedException();
		}

		return result;
	}
}

partial struct ClrJson {
	public ClrJson Remove(ClrJsonPath jsonPath) => this.RemoveFirst(1, jsonPath);

	public ClrJson RemoveFirst(int number, ClrJsonPath jsonPath) => this.RemoveCore(jsonPath, list => ReturnFirst(number, list));
	private static IList<T> ReturnFirst<T>(int number, IList<T> list) {
		if (number < 1) { return null; }
		if (list is null || list.Count <= number) { return list; }

		var result = new List<T>(number);
		int counter = 0;
		foreach (var item in list) {
			result.Add(item);
			counter++;
			if (counter >= number) { break; }
		}

		return result;
	}

	public ClrJson RemoveLast(int number, ClrJsonPath jsonPath) => this.RemoveCore(jsonPath, list => ReturnLast(number, list));
	private static IList<T> ReturnLast<T>(int number, IList<T> list) {
		if (number < 1) { return null; }
		if (list is null || list.Count <= number) { return list; }

		var result = new T[number];

		var min = Math.Max(0, list.Count - number);

		int counter = 0;
		for (var i = list.Count - 1; i >= min; i--) {
			result[counter] = list[i];
			counter++;
		}

		return result;
	}

	public ClrJson RemoveAll(ClrJsonPath jsonPath) => this.RemoveCore(jsonPath, (Func<IList<JToken>, IList<JToken>>)null);

	private ClrJson RemoveCore(ClrJsonPath jsonPath, Func<IList<JToken>, IList<JToken>> removeTokensFilter) {
		var root = this.content;
		if (root is null) {
			// this IS NULL
			return this;
		}

		var path = jsonPath.Str;
		if (string.IsNullOrWhiteSpace(path)) {
			// returns root
			return this;
		}

		var node = root as JContainer;
		if (node is null) {
			return this;
		}

		var result = new ClrJson(node.DeepClone());

		IList<JToken> tokens = new List<JToken>();
		foreach (var token in result.content.SelectTokens(path)) {
			tokens.Add(token);
		}
		tokens = removeTokensFilter is null ? tokens : removeTokensFilter(tokens);

		if (!(tokens is null)) {
			foreach (var token in tokens) {
				token.Remove();
			}
		}

		return result;
	}
}

partial struct ClrJson {
	public ClrJson Merge(ClrJson other) {
		return this.UnionMerge(other);
	}

	public ClrJson UnionMerge(ClrJson other) {
		var settings = new JsonMergeSettings();
		settings.MergeArrayHandling = MergeArrayHandling.Union;
		//settings.MergeNullValueHandling = MergeNullValueHandling.Merge;

		return this.Merge(other, settings);
	}

	public ClrJson ConcatMerge(ClrJson other) {
		var settings = new JsonMergeSettings();
		settings.MergeArrayHandling = MergeArrayHandling.Concat;
		settings.MergeNullValueHandling = MergeNullValueHandling.Merge;

		return this.Merge(other, settings);
	}

	public ClrJson IndexMerge(ClrJson other) {
		var settings = new JsonMergeSettings();
		settings.MergeArrayHandling = MergeArrayHandling.Merge;
		settings.MergeNullValueHandling = MergeNullValueHandling.Merge;

		return this.Merge(other, settings);
	}

	public ClrJson ReplaceMerge(ClrJson other) {
		var settings = new JsonMergeSettings();
		settings.MergeArrayHandling = MergeArrayHandling.Replace;
		settings.MergeNullValueHandling = MergeNullValueHandling.Merge;

		return this.Merge(other, settings);
	}

	private ClrJson Merge(ClrJson other, JsonMergeSettings settings) {
		if (this.IsNull) { return other; }
		if (other.IsNull) { return this; }

		var selfRoot = this.content as JContainer;
		var otherRoot = other.content as JContainer;

		if (null == selfRoot || null == otherRoot) {
			throw new InvalidOperationException("Can not merge json which is not container.");
		}

		var result = (JContainer)selfRoot.DeepClone();
		result.Merge(other, settings);

		return new ClrJson(result);
	}
}
#endregion Modification methods

#region IBinarySerialize implementation
partial struct ClrJson : IBinarySerialize {
	public void Read(BinaryReader r) {
		var version = r.ReadInt32();
		if (0 == version) {
			// null
			this.content = null;
			return;
		}

		if (1 != version) {
			throw new NotSupportedException($"The version ({version}) is not supported.");
		}

		var bKind = r.ReadByte();
		var kind = (ClrJsonKind)bKind;

		if (kind == ClrJsonKind.Null) {
			this.content = null;
		}
		else if (kind == ClrJsonKind.True) {
			this.content = new JValue(true);
		}
		else if (kind == ClrJsonKind.False) {
			this.content = new JValue(false);
		}
		else if (kind == ClrJsonKind.Byte) {
			var v = r.ReadByte();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.SignedByte) {
			var v = r.ReadSByte();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.Int16) {
			var v = r.ReadInt16();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.UnsignedInt16) {
			var v = r.ReadUInt16();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.Int32) {
			var v = r.ReadInt32();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.UnsignedInt32) {
			var v = r.ReadUInt32();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.Int64) {
			var v = r.ReadInt64();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.UnsignedInt64) {
			var v = r.ReadUInt64();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.Float) {
			var v = r.ReadSingle();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.Double) {
			var v = r.ReadDouble();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.Decimal) {
			var v = r.ReadDecimal();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.DateTime) {
			var ticks = r.ReadInt64();
			var dtmKind = r.ReadInt32();
			var v = new DateTime(ticks, (DateTimeKind)dtmKind);
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.TimeSpan) {
			var ticks = r.ReadInt64();
			var v = new TimeSpan(ticks);
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.String) {
			var v = r.ReadString();
			this.content = new JValue(v);
		}
		else if (kind == ClrJsonKind.Object || kind == ClrJsonKind.Array) {
			var str = r.ReadString();
			this.content = Parse(str).content;
		}
		else {
			throw new NotSupportedException($"The kind ({kind}) is not supported.");
		}
	}

	public void Write(BinaryWriter w) {
		var root = this.content;
		if (root is null) {
			w.Write(0);
			return;
		}

		w.Write(1); // version 1

		if (this.IsNull) {
			w.Write((byte)ClrJsonKind.Null);
			return;
		}

		if (root is JValue v) {
			var vt = v.Type;
			var value = v.Value;
			if (value is null) {
				w.Write((byte)ClrJsonKind.Null);
			}
			else if (value is bool bl) {
				var bv = bl ? ClrJsonKind.True : ClrJsonKind.False;
				w.Write((byte)bv);
			}
			else if (value is byte b) {
				w.Write((byte)ClrJsonKind.Byte);
				w.Write(b);
			}
			else if (value is sbyte sb) {
				w.Write((byte)ClrJsonKind.SignedByte);
				w.Write(sb);
			}
			else if (value is short s) {
				w.Write((byte)ClrJsonKind.Int16);
				w.Write(s);
			}
			else if (value is ushort us) {
				w.Write((byte)ClrJsonKind.UnsignedInt16);
				w.Write(us);
			}
			else if (value is int i) {
				w.Write((byte)ClrJsonKind.Int32);
				w.Write(i);
			}
			else if (value is uint ui) {
				w.Write((byte)ClrJsonKind.UnsignedInt32);
				w.Write(ui);
			}
			else if (value is long l) {
				w.Write((byte)ClrJsonKind.Int64);
				w.Write(l);
			}
			else if (value is ulong ul) {
				w.Write((byte)ClrJsonKind.UnsignedInt64);
				w.Write(ul);
			}
			else if (value is float f) {
				w.Write((byte)ClrJsonKind.Float);
				w.Write(f);
			}
			else if (value is double d) {
				w.Write((byte)ClrJsonKind.Double);
				w.Write(d);
			}
			else if (value is decimal m) {
				w.Write((byte)ClrJsonKind.Decimal);
				w.Write(m);
			}
			else if (value is DateTime dtm) {
				w.Write((byte)ClrJsonKind.DateTime);
				w.Write(dtm.Ticks);
				w.Write((int)dtm.Kind);
			}
			else if (value is TimeSpan ts) {
				w.Write((byte)ClrJsonKind.TimeSpan);
				w.Write(ts.Ticks);
			}
			else {
				var strValue = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}", value);
				w.Write((byte)ClrJsonKind.String);
				w.Write(strValue);
			}
		}
		else if (root is JArray) {
			w.Write((byte)ClrJsonKind.Array);
			var str = root.ToString();
			w.Write(str);
		}
		else if (root is JObject) {
			w.Write((byte)ClrJsonKind.Object);
			var str = root.ToString();
			w.Write(str);
		}
		else {
			throw new NotSupportedException($"The ({root.GetType().FullName}) type is not supported.");
		}
	}
}
#endregion IBinarySerialize implementation