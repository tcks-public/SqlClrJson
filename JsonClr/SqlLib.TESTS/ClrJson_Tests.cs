﻿using System;
using System.IO;
using Microsoft.SqlServer.Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SqlLib.TESTS {
	[TestClass]
	public class ClrJson_Tests {
		private T Reserialize<T>(T obj) where T : IBinarySerialize, new() {
			using (var mstrm = new MemoryStream()) {
				var bw = new BinaryWriter(mstrm);
				obj.Write(bw);
				bw.Flush();

				mstrm.Position = 0;
				var br = new BinaryReader(mstrm);
				var result = new T();
				result.Read(br);
				return result;
			}
		}

		[TestMethod]
		public void CanParse() {
			var json = ClrJson.Parse(@"{""title"":""my title""}");
			var q = ClrJsonPath.Parse("$.title");

			var tmpRslt = json.Query(q);
			var rslt = Reserialize(tmpRslt);

			return;
		}

		[TestMethod]
		public void CanParse_2() {
			var json = ClrJson.Parse(@"{""blah"":55}");
			var q = ClrJsonPath.Parse("$.blah");

			var tmpRslt = json.Query(q);
			var rslt = Reserialize(tmpRslt);

			var str = rslt.Str;
			Assert.AreEqual("55", str);
		}

		[TestMethod]
		public void CanRemoveAll() {
			var json = ClrJson.Parse(@"{""blah"":55,""dir"":[{""name"":""a""},{""name"":""b""},{""name"":""c""},{""name"":""d""},{""name"":""e""}]}");

			var path = new ClrJsonPath("$.dir[?(@.name > 'b')]");
			var result = json.RemoveAll(path);

			return;
		}

		[TestMethod]
		public void CanAddItemToExistingArray() {
			var json = ClrJson.Parse(@"{""blah"":55,""dir"":[{""name"":""a""},{""name"":""b""},{""name"":""c""},{""name"":""d""},{""name"":""e""}]}");

			var newItem = ClrJson.Parse(@"{""name"":""asdf""}");

			var path = new ClrJsonPath("$.dir");
			var result = json.Add(path.Str, newItem.Str);

			return;
		}

		[TestMethod]
		public void CanAddItemToMissingArray() {
			var json = ClrJson.Parse(@"{""blah"":55}");

			var newItem = ClrJson.Parse(@"{""name"":""asdf""}");

			var path = new ClrJsonPath("$.dir");
			var result = json.Add(path.Str, newItem.Str);

			return;
		}

		[TestMethod]
		public void CanAddItemToMissingArray_2() {
			var json = ClrJson.Parse(@"{""blah"":55}");

			var newItem = ClrJson.Parse(@"{""name"":""qwer""}");

			var path = new ClrJsonPath("$.dir.asdf");
			var result = json.Add(path.Str, newItem.Str);

			return;
		}

		[TestMethod]
		public void CanAddItemToMissingArray_3() {
			var json = ClrJson.Parse(@"{""blah"":55}");

			var newItem = ClrJson.Parse(@"{""name"":""qwer""}");

			var path = new ClrJsonPath("$.dir.sub_dir.one.two.three.four");
			var result = json.Add(path.Str, newItem.Str);

			return;
		}

		[TestMethod]
		public void CanAddNullToExistingArray() {
			var json = ClrJson.Parse(@"{""blah"":55,""dir"":[{""name"":""a""},{""name"":""b""},{""name"":""c""},{""name"":""d""},{""name"":""e""}]}");

			var newItem = ClrJson.Null;

			var path = new ClrJsonPath("$.dir");
			var result = json.Add(path.Str, newItem.Str);

			return;
		}

		[TestMethod]
		public void CanAddNullToNullArray() {
			var json = ClrJson.Parse(@"{""blah"":55,""dir"":null}");

			var newItem = ClrJson.Null;

			var path = new ClrJsonPath("$.dir");
			var result = json.Add(path.Str, newItem.Str);

			return;
		}

		[TestMethod]
		public void CanAddItemToNullArray() {
			var json = ClrJson.Parse(@"{""blah"":55,""dir"":null}");

			var newItem = ClrJson.Parse(@"{""name"":""asdf""}");

			var path = new ClrJsonPath("$.dir");
			var result = json.Add(path.Str, newItem.Str);

			return;
		}

		[TestMethod]
		public void CanNotAddItemToString() {
			var json = ClrJson.Parse(@"{""blah"":55,""dir"":""my string""}");

			var newItem = ClrJson.Parse(@"{""name"":""asdf""}");

			var path = new ClrJsonPath("$.dir");

			Exception thrownException = null;
			ClrJson result = ClrJson.Null;
			try {
				result = json.Add(path.Str, newItem.Str);
			}
			catch (Exception exc) {
				thrownException = exc;
			}

			Assert.IsNotNull(thrownException);
			Assert.IsInstanceOfType(thrownException, typeof(NotSupportedException));

			return;
		}

		[TestMethod]
		public void EnsureWorksWithArrayIndexes_IfExists() {
			var json = ClrJson.Parse(@"{""blah"":55, ""arr_one"":[null,null,null]}");
			var path = ClrJsonPath.Parse("$.arr_one[2]");
			var result = json.Ensure(path);

			return;
		}

		[TestMethod]
		public void EnsureWorksWithArrayIndexes_IfDontExists() {
			var json = ClrJson.Parse(@"{""blah"":55}");
			var path = ClrJsonPath.Parse("$.arr_one[2]");
			var result = json.Ensure(path);

			return;
		}

		[TestMethod]
		public void EnsureWorksWithArrayIndexes_IfDontExists_2() {
			var json = ClrJson.Parse(@"{""blah"":55}");
			var path = ClrJsonPath.Parse("$.arr_one[2].two");
			var result = json.Ensure(path);

			return;
		}

		[TestMethod]
		public void EnsureWorksWithArrayIndexes_IfArrayIsEmpty() {
			var json = ClrJson.Parse(@"{""blah"":55, ""arr_one"":[]}");
			var path = ClrJsonPath.Parse("$.arr_one[0]");
			var result = json.Ensure(path);

			return;
		}

		[TestMethod]
		public void EnsureWorksWithArrayIndexes_IfArrayIsEmpty_2() {
			var json = ClrJson.Parse(@"{""blah"":55, ""arr_one"":[]}");
			var path = ClrJsonPath.Parse("$.arr_one[5]");
			var result = json.Ensure(path);

			return;
		}

		[TestMethod]
		public void EnsureWorksWithArrayIndexes_IfArrayInArrayIsEmpty_3() {
			var json = ClrJson.Parse(@"{""blah"":55, ""arr_one"":[[],[]]}");
			var path = ClrJsonPath.Parse("$.arr_one[1][2]");
			var result = json.Ensure(path);

			return;
		}

		[TestMethod]
		public void EnsureWorksWithArrayIndexes_IfArrayInArrayIsEmpty_4() {
			var json = ClrJson.Parse(@"{""blah"":55, ""arr_one"":[[],[]]}");
			var path = ClrJsonPath.Parse("$.arr_one[1][2].two.three[3].four");
			var result = json.Ensure(path);

			return;
		}

		[TestMethod]
		public void SetWorks_IfPathExits() {
			var json = ClrJson.Parse(@"{""blah"":55, ""arr_one"":[[],[4]]}");
			var path = ClrJsonPath.Parse("$.arr_one[1][0]");
			var result = json.Set(path.Str, 13);

			return;
		}

		[TestMethod]
		public void SetWorks_WithJson_IfPathExits() {
			var json = ClrJson.Parse(@"{""blah"":55, ""arr_one"":[[],[4]]}");
			var path = ClrJsonPath.Parse("$.arr_one[1][0]");

			var item = ClrJson.Parse(@"{""name"":""Alice""}");
			var result = json.Set(path.Str, item);

			return;
		}

		[TestMethod]
		public void SetWorks_IfPathDontExits() {
			var json = ClrJson.Parse(@"{""blah"":55}");
			var path = ClrJsonPath.Parse("$.arr_one[1][0]");
			var result = json.Set(path.Str, 13);

			return;
		}

		[TestMethod]
		public void SetWorks_WithJson_IfPathDontExits() {
			var json = ClrJson.Parse(@"{""blah"":55}");
			var path = ClrJsonPath.Parse("$.arr_one[1][0]");

			var item = ClrJson.Parse(@"{""name"":""Alice""}");
			var result = json.Set(path.Str, item);

			return;
		}

		[TestMethod]
		public void Merge_WithNullNull() {
			var json_A = ClrJson.Null;
			var json_B = ClrJson.Null;

			var result = json_A.Merge(json_B);
			Assert.IsTrue(result.IsNull);
		}

		[TestMethod]
		public void Merge_WithNullEmpty() {
			var json_A = ClrJson.Null;
			var json_B = ClrJson.Parse("{}");

			var result = json_A.Merge(json_B);
			Assert.IsFalse(result.IsNull);
			Assert.AreEqual("{}", result.ToString());
		}

		[TestMethod]
		public void Merge_WithEmptyNull() {
			var json_A = ClrJson.Parse("{}");
			var json_B = ClrJson.Null;

			var result = json_A.Merge(json_B);
			Assert.IsFalse(result.IsNull);
			Assert.AreEqual("{}", result.ToString());
		}

		[TestMethod]
		public void Merge_WithEmptyEmpty() {
			var json_A = ClrJson.Parse("{}");
			var json_B = ClrJson.Parse("{}");

			var result = json_A.Merge(json_B);
			Assert.IsFalse(result.IsNull);
			Assert.AreEqual("{}", result.ToString());
		}

		[TestMethod, Ignore]
		public void Merge_WithEmptyNonEmpty() {
			var json_A = ClrJson.Parse("{}");
			var json_B = ClrJson.Parse("{\"a\":5,\"b\":{\"name\":\"Alice\"}}");

			var result = json_A.Merge(json_B);
			Assert.IsFalse(result.IsNull);
			Assert.AreEqual("{\"a\":5,\"b\":{\"name\":\"Alice\"}}", result.ToString().Replace(" ", ""));
		}

		[TestMethod]
		public void Query_WithConditionWorks() {
			var json = ClrJson.Parse("{\"items\":[{\"name\":\"Car\", \"price\":10000}]}");

			var result0 = json.Query(ClrJsonPath.Parse("$.items[?(@.price < 20000)]"));
			var result1 = json.Query(ClrJsonPath.Parse("$.items[?(@.price > 20000)]"));

			Assert.Inconclusive();
		}

		[TestMethod]
		public void ClearNulls_Works() {
			{
				var json = ClrJson.Parse("[]");
				var result = json.ClearNulls("$");
				Assert.AreEqual(0, result.Length);
			}

			{
				var json = ClrJson.Parse("[null]");
				var result = json.ClearNulls("$");
				Assert.AreEqual(0, result.Length);
			}

			{
				var json = ClrJson.Parse("[null,1,null,2]");
				var result = json.ClearNulls("$");
				Assert.AreEqual(2, result.Length);
			}

			{
				var json = ClrJson.Parse("{items:[]}");
				var result = json.ClearNulls("$.items");
				Assert.AreEqual(0, result.Q("$.items").Length);
			}

			{
				var json = ClrJson.Parse("{items:[null]}");
				var result = json.ClearNulls("$.items");
				Assert.AreEqual(0, result.Q("$.items").Length);
			}

			{
				var json = ClrJson.Parse("{items:[null,1,null,2]}");
				var result = json.ClearNulls("$.items");
				Assert.AreEqual(2, result.Q("$.items").Length);
			}
		}
	}
}
